package com.example.levua.defes.model;

/**
 * Created by admin on 11/18/2017.
 */

public class Order {
    private int hinh;
    private String ten;
    private int soluong;
    private int giatien;

    public Order(int hinh, String ten, int soluong, int giatien) {
        this.hinh = hinh;
        this.ten = ten;
        this.soluong = soluong;
        this.giatien = giatien;
    }

    public int getHinh() {
        return hinh;
    }

    public void setHinh(int hinh) {
        this.hinh = hinh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public int getGiatien() {
        return giatien;
    }

    public void setGiatien(int giatien) {
        this.giatien = giatien;
    }
}
