package com.example.levua.defes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.levua.defes.R;
import com.example.levua.defes.model.Order;

import java.util.List;

/**
 * Created by admin on 11/18/2017.
 */

public class OrderAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Order> orderList;

    public OrderAdapter(Context context, int layout, List<Order> orderList) {
        this.context = context;
        this.layout = layout;
        this.orderList = orderList;
    }

    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class ViewHolder{
        ImageView imgHinh;
        TextView txtTen,txtSoLuong,txtGia;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            viewHolder = new ViewHolder();
            viewHolder.txtTen = (TextView) view.findViewById(R.id.textViewName);
            viewHolder.txtSoLuong = (TextView) view.findViewById(R.id.textViewSoLuong);
            viewHolder.imgHinh = (ImageView) view.findViewById(R.id.imageView);
            viewHolder.txtGia = (TextView) view.findViewById(R.id.textViewGia);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        Order order = orderList.get(i);
        viewHolder.txtTen.setText(order.getTen());
        viewHolder.txtSoLuong.setText(order.getSoluong());
        viewHolder.imgHinh.setImageResource(order.getHinh());
        viewHolder.txtGia.setText(order.getGiatien());
        return view;
    }
}
