package com.example.levua.defes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.levua.defes.R;
import com.example.levua.defes.adapters.ComboAdapter;
import com.example.levua.defes.model.Combo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConfirmActivity extends AppCompatActivity {
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;
    Button bt_payMent;
    ArrayList<Combo> comboList ;
    ComboAdapter adapter;
    ListView lvCombo;
   // TextView txtEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        //txtEdit = (TextView) findViewById(R.id.edtEDIT);
        comboList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Combo combo = new Combo("", "", "", "");
            comboList.add(combo);
        }
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("list");
        for (int i = 1; i <= 4; i++) {

            databaseReference.child((String.valueOf(i))).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();

                    try {
                        int so = Integer.parseInt(map.get("sit")) - 1;
                        comboList.get(so).setAll(map.get("name"), map.get("price"), map.get("vip"), map.get("sit"));
                       // Log.d("xxx", "onDataChangexx: " + so);
                        lvCombo = (ListView) findViewById(R.id.myListView);
                        adapter = new ComboAdapter(ConfirmActivity.this,R.layout.activity_order,comboList);
                        lvCombo.setAdapter(adapter);
                    } catch (Throwable throwable) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

//        lvCombo = (ListView) findViewById(R.id.myListView);
//        adapter = new ComboAdapter(ConfirmActivity.this,R.layout.activity_order,comboList);
//        lvCombo.setAdapter(adapter);
        bt_payMent = findViewById(R.id.btnPayment);
        bt_payMent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.btnPayment:
                        Intent intent = new Intent(ConfirmActivity.this, PaymentActivity.class);
//                        Bundle bundle = new Bundle();
//                       for(int i=1;i<=4;i++){
//                           bundle.putSerializable("data",comboList.get(i));
//                       }
//                       intent.putExtras(bundle);
                        startActivity(intent);
                        break;
                }
            }
        });
    }
}
