package com.example.levua.defes.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by levua on 11/18/2017.
 */

public class TableModel {
    public String floor;
    public String num;
    public String status;
    public String vip;
    public String sit;

    public TableModel() {
    }



    public TableModel(String floor, String num, String status, String vip, String sit) {
        this.floor = floor;
        this.num = num;
        this.status = status;
        this.vip = vip;
        this.sit = sit;
    }

    public void setAll(String floor, String num, String status, String vip, String sit) {
        this.floor = floor;
        this.num = num;
        this.status = status;
        this.vip = vip;
        this.sit = sit;
    }


}
