package com.example.levua.defes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.levua.defes.model.TableModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Table extends AppCompatActivity implements View.OnClickListener {

    private Button bt_1;
    private Button bt_2;
    private Button bt_3;
    private Button bt_4;
    private Button bt_5;
    private Button bt_6;
    private Button bt_7;
    private Button bt_8;
    private Button bt_9;
    private Button bt_10;

    ArrayList<TableModel> tableModelList = null;
    ArrayList<Button> buttonArrayList;

    Intent intent = new Intent(this, BookActivity.class);


//    private DatabaseReference databaseReference;
//    private FirebaseDatabase firebaseDatabase;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private String TAG = "hihi";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("table");
        buttonArrayList = new ArrayList<>();

        setUpUI();
        tableModelList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TableModel tableModel = new TableModel("", "", "", "", "");

            tableModelList.add(tableModel);
        }

//        firebaseDatabase = FirebaseDatabase.getInstance();
//        databaseReference = firebaseDatabase.getReference("table");
//
//        databaseReference.child("data").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                List<TableModel> modelList = new ArrayList<>();
//
//                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
//                    TableModel tableModel = dataSnapshot.getValue(TableModel.class);
//                    Log.d(TAG, "onDataChange: " + dataSnapshot.getValue());
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        for (int i = 1; i <= 10; i++) {
            databaseReference.child("data").child(String.valueOf(i)).addValueEventListener(new ValueEventListener() {
                public static final String TAG = "Xxxx";

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                    try {
                        int so = Integer.parseInt(map.get("sit")) - 1;
//                        System.out.println("123: " + tableModelList.get(2).num);
                        Log.d(TAG, "onDataChangesssssss: " + (so - 1));


                        tableModelList.get(so).setAll(map.get("floor"), map.get("num"), map.get("status"), map.get("vip"), map.get("sit"));
                        if (Integer.parseInt(tableModelList.get(so).status) == 1) {
                            buttonArrayList.get(so).setBackground(getResources().getDrawable(R.color.colorInvalid));
                        } else if (Integer.parseInt(tableModelList.get(so).status) == 2) {
                            buttonArrayList.get(so).setBackground(getResources().getDrawable(R.color.colorAvailable));
                        } else {
                            buttonArrayList.get(so).setBackground(getResources().getDrawable(R.color.colorSelected));
                        }
                    } catch (Exception e) {

                    }
                }


//            }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    private void setUpUI() {

        bt_1 = findViewById(R.id.bt_1);
        bt_2 = findViewById(R.id.bt_2);
        bt_3 = findViewById(R.id.bt_3);
        bt_4 = findViewById(R.id.bt_4);
        bt_5 = findViewById(R.id.bt_5);
        bt_6 = findViewById(R.id.bt_6);
        bt_7 = findViewById(R.id.bt_7);
        bt_8 = findViewById(R.id.bt_8);
        bt_9 = findViewById(R.id.bt_9);
        bt_10 = findViewById(R.id.bt_10);

        buttonArrayList.add(bt_1);
        buttonArrayList.add(bt_2);
        buttonArrayList.add(bt_3);
        buttonArrayList.add(bt_4);
        buttonArrayList.add(bt_5);
        buttonArrayList.add(bt_6);
        buttonArrayList.add(bt_7);
        buttonArrayList.add(bt_8);
        buttonArrayList.add(bt_9);
        buttonArrayList.add(bt_10);

        bt_1.setOnClickListener(this);
        bt_2.setOnClickListener(this);
        bt_3.setOnClickListener(this);
        bt_4.setOnClickListener(this);
        bt_5.setOnClickListener(this);
        bt_6.setOnClickListener(this);
        bt_7.setOnClickListener(this);
        bt_8.setOnClickListener(this);
        bt_9.setOnClickListener(this);
        bt_10.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_1:
//                Intent i = new Intent(Table.this, DialogActivity.class);
//                startActivity(i);
                setUpButton(bt_1);

                break;

            case R.id.bt_2:

                setUpButton(bt_2);
                break;

            case R.id.bt_3:
                setUpButton(bt_3);
                break;

            case R.id.bt_4:
                setUpButton(bt_4);
                break;

            case R.id.bt_5:
                setUpButton(bt_5);
                break;

            case R.id.bt_6:
                setUpButton(bt_6);
                break;

            case R.id.bt_7:
                setUpButton(bt_7);
                break;

            case R.id.bt_8:
                setUpButton(bt_8);
                break;

            case R.id.bt_9:
                setUpButton(bt_9);
                break;

            case R.id.bt_10:
                setUpButton(bt_10);
                break;

        }
    }

    private void setUpButton(final Button button) {
        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.activity_dialog);
        dialog.setCancelable(false);

        Button bt_cancel = dialog.findViewById(R.id.bt_cancel);
        View bt_confirm = dialog.findViewById(R.id.bt_Confirm);

        TextView tv_table = dialog.findViewById(R.id.tv_table);
        TextView tv_floor = dialog.findViewById(R.id.tv_floor);
        TextView tv_num = dialog.findViewById(R.id.tv_num);
        TextView tv_vip = dialog.findViewById(R.id.tv_vip);

        tv_floor.setText("Floor: " + tableModelList.get(buttonArrayList.indexOf(button)).floor);
        tv_num.setText("Number of seat: " + tableModelList.get(buttonArrayList.indexOf(button)).num);
        tv_vip.setText("V.I.P table: " + tableModelList.get(buttonArrayList.indexOf(button)).vip);
        tv_table.setText((buttonArrayList.indexOf(button) + 1) + "");

        if (Integer.parseInt(tableModelList.get(buttonArrayList.indexOf(button)).status) != 1 ) {
            bt_confirm.setVisibility(View.GONE);
        }

        bt_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseReference.child("data").child(String.valueOf(buttonArrayList.indexOf(button) + 1))
                        .child("status").setValue("2");
                dialog.cancel();
                

            }
        });
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
    }
}



