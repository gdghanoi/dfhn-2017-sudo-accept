package com.example.levua.defes.model;

import java.io.Serializable;

/**
 * Created by levua on 11/18/2017.
 */

public class Combo implements Serializable{
    public String name;
    public String price;
    public String vip;
    public String sit;
//    public int soluong;

    public Combo(String name, String price, String vip, String sit) {
        this.name = name;
        this.price = price;
        this.vip = vip;
        this.sit = sit;
//        this.soluong = soluong;
    }

    public void setAll(String name, String price, String vip, String sit) {
        this.name = name;
        this.price = price;
        this.vip = vip;
        this.sit = sit;
//        this.soluong = soluong;
    }

    public String getName() {
        return name;
    }

//    public int getSoluong() {
//        return soluong;
//    }
//
//    public void setSoluong(int soluong) {
//        this.soluong = soluong;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getSit() {
        return sit;
    }

    public void setSit(String sit) {
        this.sit = sit;
    }
}
