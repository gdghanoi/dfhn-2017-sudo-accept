package com.example.levua.defes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.levua.defes.R;
import com.example.levua.defes.model.Combo;

import java.util.List;

/**
 * Created by levua on 11/18/2017.
 */

public class ComboAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    List<Combo> comboList;

    public ComboAdapter(Context context, int layout, List<Combo> comboList) {
        this.context = context;
        this.layout = layout;
        this.comboList = comboList;
    }

    @Override
    public int getCount() {
        return comboList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    class ViewHolder{
        TextView txtTen,txtGia,txtSoluong;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);
            viewHolder = new ViewHolder();
            viewHolder.txtTen = (TextView) view.findViewById(R.id.textViewName);
            viewHolder.txtGia = (TextView) view.findViewById(R.id.textViewGia);
            viewHolder.txtSoluong = (TextView) view.findViewById(R.id.textViewSoLuong);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        Combo combo = comboList.get(i);
        viewHolder.txtTen.setText(combo.getName());
        viewHolder.txtGia.setText(combo.getPrice());
//        viewHolder.txtSoluong.setText(combo.getSoluong());
        return view;
    }
}
