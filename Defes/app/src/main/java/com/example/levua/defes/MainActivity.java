package com.example.levua.defes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.levua.defes.model.TableModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText et_name;
    private EditText et_pass;
    private Button bt_login;
    private Button bt_regis;

    FirebaseAuth firebaseAuth;
    Intent intent;


    private String TAG = "xxxx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_name = findViewById(R.id.et_name);
        et_pass = findViewById(R.id.et_pass);

        bt_login = findViewById(R.id.bt_login);
        bt_regis = findViewById(R.id.bt_regis);

        firebaseAuth = FirebaseAuth.getInstance();

        bt_regis.setOnClickListener(this);
        bt_login.setOnClickListener(this);

        intent = new Intent(this, Table.class);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_regis:
                firebaseAuth.createUserWithEmailAndPassword(
                        et_name.getText().toString(),
                        et_pass.getText().toString()
                ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Register success!!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;

            case R.id.bt_login:
                firebaseAuth.signInWithEmailAndPassword(
                        et_name.getText().toString(),
                        et_pass.getText().toString()
                ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                            Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_SHORT).show();


                            startActivity(intent);

                        } else {
                            Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }
    }
}
