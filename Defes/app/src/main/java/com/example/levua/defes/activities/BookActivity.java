package com.example.levua.defes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.levua.defes.R;
import com.example.levua.defes.model.Combo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BookActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt_continue;



    ArrayList<Combo> comboList;
    private String TAG = "ahihi";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        comboList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Combo combo = new Combo("", "", "", "");
            comboList.add(combo);
        }

        bt_continue = findViewById(R.id.bt_continue);

        bt_continue.setOnClickListener(this);




    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_continue:

                Intent intent = new Intent(BookActivity.this, ConfirmActivity.class);

                startActivity(intent);
                break;
        }
    }
}
